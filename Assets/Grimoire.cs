using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Grimoire : MonoBehaviour
{
  public Player player;
  public void FireSummoned()
  {
    player.ChangeToFire();
  }
  public void IceSummoned()
  {
    player.ChangeToIce();
  }
  public void EarthSummoned()
  {
    player.ChangeToEarth();
  }
  public void NormalSummoned()
  {
    player.ChangeToNormal();
  }
}
