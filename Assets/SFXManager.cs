using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXManager : MonoBehaviour
{
  public static SFXManager Instance;
  [Header("Triggers")]
  public TriggerEvent trCast;
  public TriggerEvent trIce;
  public TriggerEvent trFire;
  public TriggerEvent trExplosion;
  public TriggerEvent trEarth;
  public TriggerEvent trOompf;
  public TriggerEvent trPunch;
  public TriggerEvent trThrowBone;
  public TriggerEvent trHeadbutt;
  public TriggerEvent trSkully;
  public TriggerEvent trGrumble;

  [Header("SFX")]
  [Header("Player")]
  public AudioClip cast;
  public AudioClip fail;
  public AudioClip ice_spell;
  public AudioClip fire_spell;
  public AudioClip earth_spell;
  public AudioClip oompf;
  public AudioClip punch;
  public AudioClip explosion;
  [Header("Ennemies")]
  public List<AudioClip> skully;
  public AudioClip throwBone;
  public AudioClip grumble;
  public AudioClip headbutt;

  private AudioSource source;

  // Start is called before the first frame update
  void Awake()
  {
    source = GetComponent<AudioSource>();

    trCast.Add(SfxCast);
    trFire.Add(SfxFire);
    trIce.Add(SfxIce);
    trEarth.Add(SfxEarth);
    trOompf.Add(SfxOompf);
    trPunch.Add(SfxPunch);
    trExplosion.Add(SfxExplosion);
    trThrowBone.Add(SfxBone);
    trHeadbutt.Add(SfxHeadbutt);
    trSkully.Add(SfxSkully);
    trGrumble.Add(SfxGrumble);
  }

  private void OnDestroy() {
    trCast.Remove(SfxCast);
    trFire.Remove(SfxFire);
    trIce.Remove(SfxIce);
    trEarth.Remove(SfxEarth);
    trOompf.Remove(SfxOompf);
    trPunch.Remove(SfxPunch);
    trExplosion.Remove(SfxExplosion);
    trThrowBone.Remove(SfxBone);
    trHeadbutt.Remove(SfxHeadbutt);
    trSkully.Remove(SfxSkully);
    trGrumble.Remove(SfxGrumble);
  }

  public void SfxCast(bool is_success)
  {
    if (is_success)
      source.PlayOneShot(cast);
    else
      source.PlayOneShot(fail);
  }

  public void SfxIce(bool osef)
  {
    source.PlayOneShot(ice_spell);
  }
  public void SfxFire(bool osef)
  {
    source.PlayOneShot(fire_spell);
  }
  public void SfxEarth(bool osef)
  {
    source.PlayOneShot(earth_spell);
  }
  public void SfxOompf(bool osef)
  {
    source.PlayOneShot(oompf);
  }
  public void SfxPunch(bool osef)
  {
    source.PlayOneShot(punch);
  }
  public void SfxExplosion(bool osef)
  {
    source.PlayOneShot(punch);
  }
  public void SfxBone(bool osef)
  {
    source.PlayOneShot(throwBone);
  }
  public void SfxHeadbutt(bool osef)
  {
    source.PlayOneShot(headbutt);
  }
  public void SfxSkully(bool osef)
  {
    source.PlayOneShot(skully[Random.Range(0,skully.Count)]);
  }
  public void SfxGrumble(bool osef)
  {
    source.PlayOneShot(grumble);
  }
}
