using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour {
  public static MusicManager Instance { get; private set; }
  // Start is called before the first frame update

  private Coroutine latestCoroutine;

  public AudioSource track1;
  public AudioSource track2;
  [SerializeField] bool currentlyPlayingFirst = true;

  public int age = 0;
  public bool fadeOut = false;
  [Header("BGM")]
  public AudioClip IceFight;
  public AudioClip FireFight;
  public AudioClip EarthFight;
  public AudioClip BossFight;
  public AudioClip LightFight;
  public AudioClip Ambient;
  public enum Track { Ice, Fire, Earth, Boss, Light, Ambient };

  void Start() {
    track1.playOnAwake = false;
    track2.playOnAwake = false;

    track1.clip = Ambient;
    track1.volume = 1f;
    track1.loop = true;
    track1.Play();
  }

  void Awake() {
    if (Instance != null) {
      Debug.LogError("There cannot be many instances of AudioManager, come on !");
      Destroy(gameObject);
    }
    else {
      Instance = this;
      DontDestroyOnLoad(gameObject);
    }

    IceFight = Resources.Load<AudioClip>("Sounds/BGM/fight_ice");
    EarthFight = Resources.Load<AudioClip>("Sounds/BGM/fight_earth");
    FireFight = Resources.Load<AudioClip>("Sounds/BGM/fight_fire");
    BossFight = Resources.Load<AudioClip>("Sounds/BGM/fight_boss");
    LightFight = Resources.Load<AudioClip>("Sounds/BGM/fight_light");
    Ambient = Resources.Load<AudioClip>("Sounds/BGM/ambient_placeholder");

  }

  // Update is called once per frame
  void FixedUpdate() {
    age++;
    if (fadeOut && track1.volume > 0f) {
      float newVolume = track1.volume - (1f / 360f);
      track1.volume = Mathf.Max(newVolume, 0f);
    }
  }

  public void StopTrack() {
    track1.Stop();
    track2.Stop();
  }

  public void Transition(Track track) {
    AudioClip newclip;

    //bypass cases
    if (!Game.Instance.FightMode) {
      if ((currentlyPlayingFirst && track1.clip == Ambient)
        || (!currentlyPlayingFirst && track2.clip == Ambient))
        return;
      newclip = Ambient;
    }
    else {
      if (track == Track.Fire)
        newclip = FireFight;
      else if (track == Track.Ice)
        newclip = IceFight;
      else if (track == Track.Earth)
        newclip = EarthFight;
      else if (track == Track.Boss)
        newclip = BossFight;
      else if (track == Track.Light)
        newclip = LightFight;
      else
        newclip = Ambient;
    }

    if (currentlyPlayingFirst) {
      track2.clip = newclip;
      track2.Play();
      track2.volume = 0f;
      track2.loop = true;
      track2.time = (track == Track.Light) ? 0f : track1.time;
    }
    else {
      track1.clip = newclip;
      track1.Play();
      track1.volume = 0f;
      track1.loop = true;
      track1.time = (track == Track.Light) ? 0f : track2.time;
    }

    if (latestCoroutine != null) {
      StopCoroutine(latestCoroutine);
      //MuteAll();
    }
    if (currentlyPlayingFirst)
      latestCoroutine = StartCoroutine(AudioFadeOut.CrossFade(track1, track2, 0.5f));
    else
      latestCoroutine = StartCoroutine(AudioFadeOut.CrossFade(track2, track1, 0.5f));

    currentlyPlayingFirst = !currentlyPlayingFirst;
  }
}
