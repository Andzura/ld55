using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

[RequireComponent(typeof(SpriteRenderer))]
public class Fader : MonoBehaviour {

  private SpriteRenderer _spr;
  // Start is called before the first frame update
  void Awake() {
    _spr = GetComponent<SpriteRenderer>();
  }

  void Update() {
    if (Input.GetKeyDown("t")) {
      StartCoroutine(FadeToBlackCoroutine(1f));
    }

    if (Input.GetKeyDown("u")) {
      StartCoroutine(FadeToClearCoroutine(1f));
    }
  }

  public Coroutine FadeToBlack(float delay) {
    return StartCoroutine(FadeToBlackCoroutine(delay));
  }

  public Coroutine FadeToClear(float delay) {
    return StartCoroutine(FadeToClearCoroutine(delay));
  }

  IEnumerator FadeToBlackCoroutine(float delay) {
    var timer = 0f;
    while (timer < delay) {
      yield return null;
      timer += Time.deltaTime;
      _spr.color = Color.Lerp(Color.clear, Color.black, Ease.Exponential.Out(timer / delay));
      Debug.Log(_spr.color.a);
    }
    _spr.color = Color.black;

  }

  IEnumerator FadeToClearCoroutine(float delay) {
    var timer = 0f;
    if(_spr.color.a < 0.9f) {
      yield break;
    }
    while (timer < delay) {
      yield return null;
      timer += Time.deltaTime;
      _spr.color = Color.Lerp(Color.black, Color.clear, Ease.Exponential.Out(timer / delay));
    }
    _spr.color =  Color.clear;

  }
}
