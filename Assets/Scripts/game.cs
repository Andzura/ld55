using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Rendering;

public class Game : MonoBehaviour
{
  public static Game Instance { get; private set; }

  public bool FightMode = false;

  // Start is called before the first frame update
  void Awake(){
    if (Instance == null) {
      Instance = this;
    }
    else {
      Destroy(this);
    }

  }

  // Update is called once per frame
  void Update()
  {
    if (UpdateFightMode())
      EndFight();
  }


  void FixedUpdate() {
    //foreach(GameObject interactible in GetObjectsInArea(CharaPosition(), 25f)) {
    //  if(interactible.GetComponent<Rock>() != null) {
    //    interactible.GetComponent<Rock>().TriggerWakeUp();
    //  }
    //}
  }
  private bool UpdateFightMode() //return true if fightmode ended
  {
    bool prevFightMode = FightMode;
    foreach (var enemy in FindObjectsByType<Enemy>(FindObjectsInactive.Exclude, FindObjectsSortMode.None))
    {
      if (enemy is Archer && enemy.GetComponent<Archer>().enabled)
      {
        FightMode = true;
        return false;
      }
      if (enemy is Rock && enemy.GetComponent<Rock>().enabled)
      {
        FightMode = true;
        return false;
      }
    }
    FightMode = false;
    return FightMode!=prevFightMode;
  }

  public void DisableEverythingAround()
  {
    foreach (GameObject interactible in GetObjectsInArea(CharaPosition(), 25f))
    {
      interactible.SetActive(false);
    }
  }

  public void CastSpell(string spell, Vector2 target) {
    Debug.Log("casting "+ spell);
    if(spell == "") {
      return;
    }
    Spell SpellObject;

    if(spell == "ice" || spell=="tatane") {
      SpellObject = Instantiate(Resources.Load("Prefabs/Spells/" + spell), Player.GetTransform()).GetComponent<Spell>();
    } else {
      SpellObject = Instantiate(Resources.Load("Prefabs/Spells/" + spell)).GetComponent<Spell>();
    }
    SpellObject.center = CharaPosition();
    SpellObject.target = target;
  }

  public List<GameObject> GetObjectsInArea(Vector2 center,  float rayon, string tag = "Interactible") {
    GameObject[] interactibles = GameObject.FindGameObjectsWithTag(tag);
    List<GameObject> inArea = new List<GameObject>();
    foreach(GameObject interactible in interactibles) {
      Vector2 position = (Vector2)interactible.transform.position;
      if (position.IsInArea(center, rayon)) {
        inArea.Add(interactible);
      }
    }

    return inArea;
  }

  private Vector2 CharaPosition() {
    Vector3 position = GameObject.Find("Player").transform.position;

    return new Vector2(position.x, position.y);
  }

  public void LetsFight()
  {
    if (FightMode)
      return;
    MusicManager.Instance.Transition(MusicManager.Track.Light);
    FightMode = true;
  }
  public void EndFight()
  {
    MusicManager.Instance.Transition(MusicManager.Track.Ambient);
    Player.Instance.ChangeToNormal();
  }

  public void EndGame() {
    //FIXME
    // Keep only Application.quit when building
    Debug.Log("ALERTE MAXIMUM - SUPPRIMEZ MOI QUAND ON FERA LE BUILD FINAL");
    UnityEditor.EditorApplication.isPlaying = false;
    Application.Quit();
  }
}
