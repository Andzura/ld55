using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour, IDamageable {

  [Serializable]
  public struct Checkpoint {
    public Vector3 position;
    public string sceneName;
  };

  public static Player Instance { get; private set; }
  public enum ElementType { Normal, Ice, Fire, Earth }
  public ElementType Element = ElementType.Normal;

  public UnityEvent takeDamage;

  [Header("SFX Triggers")]
  public TriggerEvent trOompf;
  public TriggerEvent trPunch;
  public TriggerEvent trFire;
  public TriggerEvent trEarth;
  public TriggerEvent trIce;

  private float _health = 12;
  private float _maxHealth = 12;
  [SerializeField]
  private Checkpoint _checkpoint;

  public float Health {
    get => _health;
    set => _health = value;
  }

  public float MaxHealth  {
    get => _maxHealth;
    set => _maxHealth = value;
  }

  public float punchDmg=0.5f;
  public float speedMult=1f;
  public float damageRes=1.5f;
  private bool _canTakeDamage = true;


  private void Awake() {
    // If there is an instance, and it's not me, delete myself.
    if (Instance != null) {
        Destroy(gameObject);
    }
    else {
        Instance = this;
        DontDestroyOnLoad(gameObject);
    }
  }

  private void Update()
  {
    GetComponent<Move>().CurrentMaxSpeed = 6f * speedMult;
    if(Input.GetMouseButtonUp(0))
    {
      Vector3 world_pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
      int pointing = Math.Sign((world_pos - transform.position).x);
      transform.Find("Grimoire").GetComponent<Animator>().SetInteger("Pointing", pointing);
    }
    if (Input.GetKeyDown("u"))
      Death();
  }

  public static Transform GetTransform() {
    return Instance?.transform;
  }

  public void SetCheckpoint(Checkpoint c) {
    _checkpoint = c;
  }

  public void Death() {
    GetComponent<Move>().DisableBehaviour();
    GameObject.Find("Game").GetComponent<Game>().DisableEverythingAround();
    GetComponent<Animator>().SetTrigger("Dead");
    GameObject.FindFirstObjectByType<MusicManager>().StopTrack();
    //Pause everything except animation
    //Restart at end of animation
  }

  public void ReloadPlayer() {
    StartCoroutine(delayRespawn(2f));

  }
   IEnumerator delayRespawn(float delay) {
    yield return Camera.main.GetComponentInChildren<Fader>().FadeToBlack(delay);
    Camera.main.GetComponent<Follow>().SnapFollow();
    Camera.main.GetComponent<Follow>().ClearObjects();
    Camera.main.GetComponent<Follow>().AddObjectToFollow(transform);
    ReturnToNormal();
    transform.position = _checkpoint.position;
    SceneManager.LoadScene(_checkpoint.sceneName, LoadSceneMode.Single);
    yield return new WaitForFixedUpdate();
    Camera.main.GetComponent<Follow>().StopSnapFollow();
    yield return Camera.main.GetComponentInChildren<Fader>().FadeToClear(delay);

    GetComponent<Move>().EnableBehaviour();
    _health = _maxHealth;
  }

  void IDamageable.TakeDamage(float damage) { 
    if(!_canTakeDamage) {
      Debug.Log("cannot take damage yet");
      return;
    }

    trOompf.Raise(true);
    Health -= damage * damageRes;
    if (Health <= 0) {
      Death();
    }
    else
    {
      GetComponent<Animator>().SetTrigger("Hit");
    }

    takeDamage.Invoke();
  }



  public void SpecialActionAfterDamage() {
    _canTakeDamage = true;
  }

  public SpriteRenderer Renderer() {
    return GetComponent<SpriteRenderer>();
  }

  public void StartFlashingCoroutine() {
    StartCoroutine(((IDamageable)this).Flash(0.15f, 0.8f));
  }

  //===================================
  //Link cast spell to animation
  public void CastSpell(ElementType el)
  {
    GetComponent<Animator>().SetTrigger("CastSpell");
    if (el == ElementType.Fire)
      SummonFire();
    else if (el == ElementType.Ice)
      SummonIce();
    else if (el == ElementType.Earth)
      SummonEarth();
  }

  public void CastPunch()
  {
    transform.Find("Grimoire").GetComponent<Animator>().SetTrigger("Punch");
    trPunch.Raise(true);
  }

  //==========================================================
  //Starts the transformation animation into indicated element
  private void SummonFire()
  {
    trFire.Raise(true);
    if (Element != ElementType.Fire)
      transform.Find("Grimoire").GetComponent<Animator>().SetTrigger("SummonFire");
    else
      transform.Find("Grimoire").GetComponent<Animator>().SetTrigger("CastFire");
  }
  private void SummonEarth()
  {
    trEarth.Raise(true);
    if (Element != ElementType.Earth)
      transform.Find("Grimoire").GetComponent<Animator>().SetTrigger("SummonEarth");
    else
      transform.Find("Grimoire").GetComponent<Animator>().SetTrigger("CastEarth");
  }
  private void SummonIce()
  {
    trIce.Raise(true);
    if (Element != ElementType.Ice)
      transform.Find("Grimoire").GetComponent<Animator>().SetTrigger("SummonIce");
    else
      transform.Find("Grimoire").GetComponent<Animator>().SetTrigger("CastIce");
  }
  private void ReturnToNormal()
  {
    GetComponent<Animator>().SetTrigger("ReturnToNormal");
    transform.Find("Grimoire").GetComponent<Animator>().SetTrigger("ReturnToNormal");
    ChangeToNormal();
  }

  //==============================
  //Called by Grimoire at the end of the transformation animation; Turns the player's sprite & stats into indicated element
  public void ChangeToIce()
  {
    damageRes = 1f;
    speedMult = 1f;
    punchDmg = 3f;

    GetComponent<Animator>().SetTrigger("ChangeToIce");
    Element = ElementType.Ice;
    GameObject.FindFirstObjectByType<MusicManager>().Transition(MusicManager.Track.Ice);
  }
  public void ChangeToFire()
  {
    damageRes = 2f;
    speedMult = 2f;
    punchDmg = 0.5f;

    GetComponent<Animator>().SetTrigger("ChangeToFire");
    Element = ElementType.Fire;
    GameObject.FindFirstObjectByType<MusicManager>().Transition(MusicManager.Track.Fire);
  }
  public void ChangeToEarth()
  {
    damageRes = 0.5f;
    speedMult = 0.7f;
    punchDmg = 1.5f;

    GetComponent<Animator>().SetTrigger("ChangeToEarth");
    Element = ElementType.Earth;
    GameObject.FindFirstObjectByType<MusicManager>().Transition(MusicManager.Track.Earth);
  }
  public void ChangeToNormal()
  {
    punchDmg = 0.5f;
    speedMult = 1f;
    damageRes = 1.5f;

    GetComponent<Animator>().SetTrigger("ChangeToNormal");
    Element = ElementType.Normal;
    GameObject.FindFirstObjectByType<MusicManager>().Transition(MusicManager.Track.Ambient);
  }

}
