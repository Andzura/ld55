using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Glyph", menuName = "ScriptableObjects/Glyphs", order = 1)]
public class LineObject : ScriptableObject {
  public string GlyphName;

  public Vector2[] PointsLists;
  public bool IsSpell;
  public bool IsChild;
  public List<LineObject> NextGlyphs;
}
