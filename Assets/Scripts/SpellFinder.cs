using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Jobs;
using UnityEngine;

public class SpellFinder : MonoBehaviour {
  [Header("SFX Triggers")]
  public TriggerEvent trCast;
  private class AcceptedGlyph {
    public LineObject Glyph;
    public Vector2[] Line;
  }

  private Queue<CompareJob> _jobQueue = new Queue<CompareJob>();
  private JobHandle _currentJob;
  private bool _jobStarted = false;
  private Vector2[] _jobLine;
  private AcceptedGlyph _currentGlyph;
  private List<AcceptedGlyph> _compoGlyph = new List<AcceptedGlyph>();
  private CompareJob _jobOutput;
  public static SpellFinder Instance { get; private set; }

  public LineObject GetCurrentGlyph() {

    return _currentGlyph?.Glyph;
  }

  void Awake() {
    if (Instance == null) {
      Instance = this;
    }
    else {
      Destroy(this);
    }
  }
  public void EnqueueJob(Vector2[] positions) {
    _jobQueue.Enqueue(LineComparator.CreateCompareJob(positions));
  }

  void FixedUpdate() {
    if (_jobStarted) {
      if (_currentJob.IsCompleted) {
        _currentJob.Complete();
        if (_currentGlyph != null) {
          _compoGlyph.Add(_currentGlyph);
        }
        var glyph = ResourcesLoader.Instance.GetGlyph(_jobOutput.output.ToString());
        if (glyph != null) {
          trCast.Raise(true); //SFX
          _currentGlyph = new AcceptedGlyph() {
            Glyph = glyph,
            Line = _jobLine
          };
          if (glyph.IsSpell) {
            SendSpell();
          }
        }
        else
          trCast.Raise(false); //SFX
        _jobOutput.Positions.Dispose();
        _jobOutput.output.Dispose();
        _jobStarted = false;
      }
      else {
        return;
      }
    }
    if (_jobQueue.Count > 0) {
      _jobStarted = true;
      _jobOutput  = _jobQueue.Dequeue();
      _jobLine = _jobOutput.Positions.ToArray();
      _currentJob = _jobOutput.Schedule();
    }
  }

  internal void SendSpell() {
    var spellname = "";
    Vector2 target;
    foreach (var glyph in _compoGlyph) {
      spellname += glyph.Glyph.GlyphName + " ";
    }
    if (_currentGlyph != null) {
      spellname += _currentGlyph.Glyph.GlyphName;
      target = _currentGlyph.Line.Barycentre();
    }
    else {
      return;
    }
    trCast.Raise(true);
    Game.Instance.CastSpell(spellname, target);
    //GameObject.Find("Player").GetComponent<Animator>().SetInteger("State", 1);
    _compoGlyph.Clear();
    _currentGlyph = null;
  }
}
