using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;
public class Bullet : MonoBehaviour {
  [SerializeField]
  private float _speed = 20f;

  [SerializeField]
  private float _delay = 2f;

  [SerializeField]
  private float _maxDistance = 10f;

  [SerializeField]
  private float _damage = 5f;

  [SerializeField]
  private ParticleSystem _effect;

  private Vector2 _direction;
  private float _timeAlive = 0f;

  public void SetDirection(Vector2 d) {
    _direction = d;
  }

  void Awake() {
    _timeAlive = 0f;
    StartCoroutine(BulletCoroutine());
  }

  private void FixedUpdate()
  {
    List<Collider2D> overlaps = new List<Collider2D>();
    GetComponentInChildren<BoxCollider2D>().Overlap(overlaps);
    foreach (var overlap in overlaps)
    {
      if (overlap.gameObject.GetComponent<Player>() != null)
      {
        ((IDamageable)overlap.gameObject.GetComponent<Player>()).TakeDamage(_damage);
        Destroy(gameObject);
      }
    }
  }

  protected IEnumerator BulletCoroutine() {
    var waitCoroutine = new WaitForFixedUpdate();
    bool alive = true;
    while (alive) {
      _timeAlive += Time.fixedDeltaTime;
      if (_timeAlive >= _delay) {
        Destroy(gameObject);
        alive = false;
      }
      if (
          Vector2.Distance(Camera.main.transform.position, transform.position)
          >= _maxDistance
      ) {
        Destroy(gameObject);
        alive = false;
      }
      if (alive)
        transform.position +=
            (Vector3)_direction * _speed * Time.fixedDeltaTime * Time.timeScale;
      yield return waitCoroutine;
    }
  }

  void OnCollisionEnter2D(Collision2D collision) {
    if (collision.gameObject.tag == "Player") {
      collision.gameObject.SendMessage("TakeDamage", _damage);
      Destroy(gameObject);
    }
  }
}

