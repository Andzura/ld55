using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour, IDamageable {

  private float _health;
  private float _maxHealth;
  protected bool _froze = false;

  public float Health {
    get => _health;
    set => _health = value;
  }

  public float MaxHealth  {
    get => _maxHealth;
    set => _maxHealth = value;
  }


  private void Awake() {}

  private void OnEnable()
  {
    Game.Instance.LetsFight();
  }

  private void OnDestroy()
  {
  }

  public Transform GetTransform() {
    return gameObject.transform;
  }

  public virtual void Death() {
    //TODO
    //ici on peut fair een sorte de trigger l'animation de mort de l'ennemi
    gameObject.tag = "Untagged";

    foreach (MonoBehaviour childComponent in gameObject.GetComponents<MonoBehaviour>()) {
      childComponent.enabled = false;
    }
  }
  public SpriteRenderer Renderer() {
    return GetComponent<SpriteRenderer>();
  }

  public void SpecialActionAfterDamage() {

  }

  public void StartFlashingCoroutine() {
    StartCoroutine(((IDamageable)this).Flash(0.05f, 0.2f));
  }

  public void Freeze(float duration) {
    _froze = true;
    GetComponent<InputBehaviour>().DisableBehaviour();
    StartCoroutine(DelayFreeze(duration));
  }

  private IEnumerator DelayFreeze(float duration) {
    yield return new WaitForSeconds(duration);
    _froze = false;
    GetComponent<InputBehaviour>().EnableBehaviour();
  }   
 }
