using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Archer : Enemy, IDamageable
{
  [Header("SFX Triggers")]
  public TriggerEvent trBones;
  public TriggerEvent trThrow;

  public float ShootDelay = 3.0f;
  public GameObject BulletPrefab;

  private bool _isAwaken = false;
  float runCooldown = 1f;
  public GameObject _player;

  private float _shootTimer;

  public float SkullyMaxHealth = 4f;


  [Header("Awaken Behavior")]
  [field: SerializeField, Range(5, 100f)]
  public float AwakenDistance { get; set; } = 10f;


  private void Awake()
  {
    MaxHealth = SkullyMaxHealth;
    Health = MaxHealth;
  }

  void FixedUpdate() {
    if(runCooldown>0f)
    {
      runCooldown -= Time.deltaTime;
      GetComponent<Shielder>().shouldMove = false;
    }
    else
      GetComponent<Shielder>().shouldMove = true;

    if (!_isAwaken) //Can't move if not awaken
    {
      runCooldown = 99f;
      if (Vector3.Distance(transform.position, Player.Instance.transform.position) < AwakenDistance)
        TriggerWakeUp();
      return;
    }

    if (Random.Range(0f, 1f) < 0.01)
      trBones.Raise(true);

    if (CanAttack()) {
      StartAttack();
      return;
    }

    if (_shootTimer > 0) {
      _shootTimer -= Time.deltaTime;

      BoxCollider2D selfCollider = gameObject.GetComponent<BoxCollider2D>();
      BoxCollider2D playerCollider = Player.Instance.GetComponent<BoxCollider2D>();
    }
  }

  public void TriggerWakeUp()
  {
    if (_isAwaken)
      return;
    _isAwaken = true;
    runCooldown = 1f;
    GetComponent<Animator>().SetTrigger("WakeUp");
    trBones.Raise(true);
  }

  public void TriggerDeath() //Start death animation and call parent procedure Death to kill the gameobject
  {
    GetComponent<Animator>().SetTrigger("Killed");
    Death();
  }

  public void TriggerHit()
  {
    GetComponent<Animator>().SetTrigger("Hit");
    runCooldown = 1f;
  }

  void IDamageable.TakeDamage(float dmg)
  {
    Health -= dmg;
    if (Health <= 0f)
      TriggerDeath();
    else
      TriggerHit();
  }

  public void KillObject()
  {
    Destroy(gameObject);
  }

  public bool CanAttack() {
    if (_shootTimer <= 0) {
      return true;
    }
    return false;
  }

  private void StartAttack()
  {
    GetComponent<Animator>().SetTrigger("Attack");
    runCooldown = 1.33f;
    _shootTimer = ShootDelay;
    Debug.Log(name + " starts attacking");
  }

  public void TriggerAttack()
  {
    _shootTimer = ShootDelay;
    Vector2 direction = (Player.Instance.transform.position - transform.position).normalized;
    GameObject go = Instantiate(BulletPrefab, transform);
    go.transform.SetParent(null);
    go.GetComponent<Bullet>().SetDirection(direction);
  }
}
