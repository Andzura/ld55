using UnityEngine;
using System.Collections;

public class Rock : Enemy, IDamageable {
  [Header("SFX Triggers")]
  public TriggerEvent trGrumble;
  public TriggerEvent trHeadbutt;
  public enum IsAwakenBy { Proximity, Trigger}
  [Header("Awaken Behavior")]
  public IsAwakenBy AwakenBy = IsAwakenBy.Proximity;
  [field: SerializeField, Range(5, 100f)]
  public float AwakenDistance { get; set; } = 5f;
  //public GameEvent AwakeTrigger to set

  public float RockMaxHealth = 3f;

  private bool _isAwaken = false;
  private bool _attacked = false;
  float attackCooldown = 0;
  float runCooldown = 0;
  public GameObject _player;
  private float _damage = 2;

  private void Awake() {
    MaxHealth = RockMaxHealth;
    Health = MaxHealth;
  }

  private void Update()
  {
    //DEBUG
    //if (Input.GetKeyDown("space"))
    //  TriggerHit();
    //if (Input.GetKeyDown("k"))
    //  TriggerDeath();
  }

  void FixedUpdate() {

    if (!_isAwaken) //Can't move if not awaken
    {
      runCooldown = 99f;
      if (Vector3.Distance(transform.position, Player().transform.position) < AwakenDistance)
        TriggerWakeUp();
      return;
    }

    if (CanAttack()) {
      TriggerAttack();
      return;
    }

    if(runCooldown >= 0f) {
      runCooldown -= Time.deltaTime;

      if (runCooldown < 0f)
      {
        runCooldown = 0f;
        StartFollow();
        this.gameObject.GetComponent<MoveNoRB>().CurrentMaxSpeed = 3f;
      }
      else
        StopFollow();

      BoxCollider2D selfCollider = gameObject.GetComponent<BoxCollider2D>();
      BoxCollider2D playerCollider = Player().transform.Find("Hitbox").GetComponent<BoxCollider2D>();

      if (selfCollider.IsTouching(playerCollider) && !_attacked) {
        GetComponent<Animator>().SetTrigger("Attack");
        trHeadbutt.Raise(true);
        _attacked = true;
        attackCooldown = 3f;
        ((IDamageable)Player().GetComponent<Player>()).TakeDamage(_damage);
        StopFollow();
      }
    }

    if (attackCooldown > 0) {
      attackCooldown -= Time.deltaTime;

      if(attackCooldown < 0 ) {
        attackCooldown = 0;
        _attacked = false;
        this.gameObject.GetComponent<MoveNoRB>().ResetValues();
        StartFollow();
      }
    }
  }

  public void StartFollow() {
    Debug.Log(name + " start follow");
    if(runCooldown > 0) {
      return;
    }
    this.gameObject.GetComponent<MoveNoRB>().enabled = true;
    this.gameObject.GetComponent<FollowPlayer>().enabled = true;
  }

  public void StopFollow() {
    this.gameObject.GetComponent<MoveNoRB>().enabled = false;
    this.gameObject.GetComponent<FollowPlayer>().enabled = false;
  }

  public bool CanAttack() {
    Vector2 position = gameObject.transform.localPosition;
    if (!_froze && attackCooldown == 0 && runCooldown == 0 && position.IsInArea(Player().transform.localPosition, 15f)) {
      return true;
    }
    return false;
  }

  private void TriggerAttack() {
    runCooldown = 2f;

    this.gameObject.GetComponent<MoveNoRB>().CurrentMaxSpeed = 6f;
  }

  public void TriggerWakeUp()
  {
    if (_isAwaken)
      return;
    _isAwaken = true;
    runCooldown = 1f;
    GetComponent<Animator>().SetTrigger("WakeUp");
    trGrumble.Raise(true);
  }

  public void TriggerDeath() //Start death animation and call parent procedure Death to kill the gameobject
  {
    GetComponent<Animator>().SetTrigger("Killed");
    Death();
  }

  public void TriggerHit()
  {
    GetComponent<Animator>().SetTrigger("Hit");
    runCooldown = 1f;
  }

  void IDamageable.TakeDamage(float dmg)
  {
    Health -= dmg;
    if (Health <= 0f)
      TriggerDeath();
    else
      TriggerHit();
  }

  public void KillObject()
  {
    GetComponent<Rock>().enabled = false;
    Destroy(gameObject);
  }

  private GameObject Player() {
    if (_player != null) {
      return _player;
    }
    return GameObject.Find("Player");
  }
}
