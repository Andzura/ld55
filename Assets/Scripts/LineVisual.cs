using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineVisual : MonoBehaviour {
  private bool _done = false;

  // Start is called before the first frame update
  void Awake() {

  }

  // T is called once per frame
  void FixedUpdate() {
    if (_done) {
      Destroy(gameObject);
    }
  }

  public void StartFadeOut() {
    StartCoroutine(FadeOut());
  }

  private IEnumerator FadeOut() {
    LineRenderer lineRenderer = GetComponent<LineRenderer>();
    Color startColor = lineRenderer.startColor;
    Color endColor = lineRenderer.endColor;
    while (true) {
      startColor.a -= 0.02f;
      endColor.a -= 0.02f;

      lineRenderer.startColor = startColor;
      lineRenderer.endColor = endColor;
      yield return new WaitForFixedUpdate();
      if (startColor.a <= 0) {
        _done = true;
        break;
      }
    }
  }
}
