using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class KeySpell : Spell {

  public new void FixedUpdate() {
      List<GameObject> interactibles = GameObject.Find("Game").GetComponent<Game>().GetObjectsInArea(this.center, this.rayon);
      ApplySpell(interactibles);
      Destroy(this.gameObject);
      return;
  }

  protected new void ApplySpellOnOneEntity(GameObject interactible) {
    interactible.SendMessage("KeyOpen");
  }
}