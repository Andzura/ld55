using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class TataneSpell : Spell {

  private float timer = 0.1f;
  public List<Transform> hurtpoints;

  public new void Start() {
    MoveCenter();
    this.gameObject.transform.position = new Vector3(center.x, center.y, 0);
    this.transform.parent.gameObject.GetComponent<Player>().CastPunch();
  }

  public new void FixedUpdate() {
    if(timer >= 0) {
      timer -= Time.deltaTime;

      if (timer <= 0) {
        timer = 0;
        List<GameObject> interactibles = new List<GameObject>();
        foreach(var hurtpoint in hurtpoints)
          interactibles.AddRange(GameObject.Find("Game").GetComponent<Game>().GetObjectsInArea(hurtpoint.position, 1.5f)); 
        foreach (var overlap in interactibles)
        {
          Debug.Log("Punched " + overlap.name);
          ApplySpellOnOneEntity(overlap.gameObject);
        }
        Destroy(this.gameObject);
        return;
      }
    }
  }

  private void MoveCenter()
  {    
    //flip x
    if ((target.x - center.x) < 0f)
      transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
  }

  protected new void ApplySpellOnOneEntity(GameObject interactible) {
    var damageObject = interactible.GetComponent<IDamageable>();
    if (damageObject != null)
      damageObject.TakeDamage(Player.Instance.punchDmg);
  }
}