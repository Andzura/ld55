using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveSpell : Spell {
  private float _timer = 10f;

  new void FixedUpdate() {
    _timer -= Time.fixedDeltaTime;
    if(_timer < 0 ) {
      Destroy(gameObject);
    }
  }
  private void OnTriggerEnter2D(Collider2D collision) {
    CheckpointObject co = collision.GetComponent<CheckpointObject>();
    if (co != null) {
      co.Save();
    }
  }
}
