using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class FireSpell : Spell {
  public float Duration = 1f;
  private bool isExploding = false;

  private Vector2 direction;
  private Vector2 startpoint;

  public new void Start()
  {
    Vector3 charaPosition = GameObject.Find("Player").transform.position;
    this.center = new Vector2(charaPosition.x, charaPosition.y);
    direction = (target - center).normalized;
    startpoint = center;

    this.gameObject.transform.position = new Vector3(center.x, center.y, 0);
    this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
    GameObject.Find("Player").GetComponent<Player>().CastSpell(Player.ElementType.Fire);
  }

  public new void FixedUpdate()
  {
    if (isExploding)
    {
      GetComponent<BoxCollider2D>().enabled = false;
      transform.eulerAngles = new Vector3(0f, 0f, 0f);
    }
    else
    {
      transform.eulerAngles = new Vector3(0f, 0f, Math.Sign(direction.y) * Vector2.Angle(new Vector2(1f, 0f), target - startpoint));
      center = Vector2.MoveTowards(center, center + direction, 0.5f);
      this.gameObject.transform.position = center;

      List<Collider2D> overlaps = new List<Collider2D>();
      GetComponentInChildren<BoxCollider2D>().Overlap(overlaps);
      foreach (var overlap in overlaps)
      {
        CollideWith(overlap);
      }
    }
  }

  private void CollideWith(Collider2D collision)
  {
    if (collision.gameObject.GetComponent<Collider2D>()==null)
      return;
    if (collision.gameObject.GetComponent<Player>() != null || collision.gameObject.GetComponentInParent<Player>() != null)
      return;
    Debug.Log("fireball exploded on " + collision.gameObject.name);
    List<GameObject> interactibles = GameObject.Find("Game").GetComponent<Game>().GetObjectsInArea(this.center, this.rayon);
    interactibles.AddRange(new List<GameObject>{collision.gameObject});
    GetComponent<Animator>().SetTrigger("Explode");
    isExploding = true;
    ApplySpell(interactibles);
  }

  public void Kill()
  {
    //Notify main Game Script
    Destroy(this.gameObject);
  }

  protected override void ApplySpellOnOneEntity(GameObject interactible) {
    var damageComponent = interactible.GetComponent<IDamageable>();
    if (damageComponent != null)
      damageComponent.TakeDamage(2);
    var interactObject = interactible.GetComponent<IInteractable>();
    if (interactObject != null)
      interactObject.Fire(Duration);
  }
}