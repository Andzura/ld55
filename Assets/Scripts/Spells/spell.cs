using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class Spell : MonoBehaviour {
  [HideInInspector] public Vector2 center;
  [HideInInspector] public Vector2 target;
  public float rayon;

  public void Start() {
    Vector3 charaPosition = GameObject.Find("Player").transform.localPosition;
    this.center = new Vector2(charaPosition.x, charaPosition.y);

    this.gameObject.transform.localPosition = new Vector3(center.x, center.y, 0);
    var spr = this.gameObject.GetComponent<SpriteRenderer>();
    if(spr != null)
      spr.enabled = true;
  }

  public void FixedUpdate() {

  }

  protected void ApplySpell(List<GameObject> interactibles) {
    foreach(GameObject interactible in interactibles) {

      ApplySpellOnOneEntity(interactible);
    }

  }

  protected virtual void ApplySpellOnOneEntity(GameObject interactible) {
  }

}