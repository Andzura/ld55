using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class IceSpell : Spell {
  public float Duration = 5f;

  private float timer = 1f;
  public Vector2 _center;

  public new void Start() {
    MoveCenter();
    this.gameObject.transform.position = new Vector3(center.x, center.y, 0);
    this.transform.parent.gameObject.GetComponent<Player>().CastSpell(Player.ElementType.Ice);
  }

  public new void FixedUpdate() {
    if(timer > 0) {
      timer -= Time.deltaTime;

      if (timer <= 0) {
        timer = 0;
        Destroy(this.gameObject);
        return;
      }
    }

    List<Collider2D> overlaps = new List<Collider2D>();
    GetComponentInChildren<BoxCollider2D>().Overlap(overlaps);
    foreach(var overlap in overlaps)
    {
      if (overlap.GameObject().GetComponent<InteractDoor>() != null)
        overlap.GameObject().GetComponent<InteractDoor>().Freeze(0f);
      else if (overlap.GameObject().GetComponent<Enemy>() != null)
        overlap.GameObject().GetComponent<Enemy>().Freeze(4f);
    }
  }

  private void MoveCenter()
  {
    //angle
    float angle = Vector2.Angle(target - center, new Vector2(1f, 0f));
    if (angle > 90f)
    {
      transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
      angle = -Vector2.Angle(target - center, new Vector2(-1f, 0f));
    }
    if (angle > 45f)
      angle = 45f;
    //flip y
    if ((target.y - center.y) < 0f)
      angle *= -1f;
    transform.eulerAngles = new Vector3(0f, 0f, angle);
  }

  protected new void ApplySpellOnOneEntity(GameObject interactible) {
    interactible.SendMessage("Freeze", Duration);
  }

  private void OnTriggerEnter2D(Collider2D other){
    other.gameObject.SendMessage("Freeze", Duration);
  }
}