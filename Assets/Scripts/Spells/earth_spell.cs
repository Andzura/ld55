using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class EarthSpell : Spell {

  private float timer = 2f;

  public new void Start() {
    this.center = target;
    this.gameObject.transform.position = target;
    this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
    GameObject.Find("Player").GetComponent<Player>().CastSpell(Player.ElementType.Earth);
  }

  public new void FixedUpdate() {
    if(timer > 0) {
      timer -= Time.deltaTime;

      if (timer <= 0) {
        timer = 0;

        Destroy(this.gameObject);
        return;
      } else {
        List<GameObject> interactibles = GameObject.Find("Game").GetComponent<Game>().GetObjectsInArea(this.center, this.rayon);
        ApplySpell(interactibles);
      }
    }

  }


  protected override void ApplySpellOnOneEntity(GameObject interactible) {
    Vector2 position = (Vector2)interactible.transform.position;
    float normalizedDistance = position.DistanceTo(center) / rayon;

    float speed = 0.1f - (normalizedDistance / 10);

    if (speed < 0.01f) {
      speed = 0.01f;
    } else if (speed > 0.1f) {
      speed = 0.1f;
    }

    Vector2 new_position = Vector2.MoveTowards(position, center, speed);

    interactible.transform.position = new_position;

  }
}
