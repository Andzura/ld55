using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using UnityEngine.UI;

public class LifeBarManager : MonoBehaviour {

  Player _player;

  public List<LifeItem> hearts = new List<LifeItem>();

  public void DrawHearts() {
    float palier = Player().MaxHealth / 6;
    if (Player().Health <= 0)
      return ;

    for(int i = 0; i < 3; i++) {
      if(Player().Health >= palier * ((2 * i) + 2)) {
        hearts[i].SetHeartImage("full");
      } else if (Player().Health >= palier * ((2 * i) + 1)) {
        hearts[i].SetHeartImage("half");
      } else {
        hearts[i].SetHeartImage("empty");
      }
    }
  }

  public void Start() {
    DrawHearts();
  }

  private Player Player() {
    if (_player != null) {
      return _player;
    }
    return GameObject.Find("Player").GetComponent<Player>();
  }

  public void ClearHearts() {
    foreach(Transform t in transform) {
      Destroy(t.gameObject);
    }
    hearts = new List<LifeItem>();
  }
}
