using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LifeItem : MonoBehaviour {
  Image heartImage;
  public Sprite fullHeart, halfHeart, emptyHeart;

  public void Awake() {
    heartImage = GetComponent<Image>();
  }
  // Start is called before the first frame update
  void Start() {

  }

  // Update is called once per frame
  void Update() {

  }

  public void SetHeartImage(string status) {
    switch(status) {
      case "empty":
        heartImage.sprite = emptyHeart;
        break;
      case "half":
        heartImage.sprite = halfHeart;
        break;
      case "full":
        heartImage.sprite = fullHeart;
        break;
    }
  }
}

