using System.Collections;
using System.Collections.Generic;
using UnityEngine;


using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

  [CustomEditor(typeof(LoadTrigger))]
  public class LevelChangeCustomEditor : Editor {
    protected virtual void OnSceneGUI(SceneView sv) {
      LoadTrigger lt = target as LoadTrigger;
      Vector3 pos = lt.transform.position;

      float size = HandleUtility.GetHandleSize(lt.Position) * 0.5f;
      float snap = 0.1f;
      Vector3 handleDirection = Vector3.back;

      EditorGUI.BeginChangeCheck();
      Vector3 newTargetPosition = Handles.PositionHandle(lt.Position, Quaternion.identity);
      if (EditorGUI.EndChangeCheck()) {
        lt.Position = newTargetPosition;
      }
    }

    void OnEnable() {
      SceneView.duringSceneGui += OnSceneGUI;
    }

    void OnDisable() {
      SceneView.duringSceneGui -= OnSceneGUI;
    }
  }
