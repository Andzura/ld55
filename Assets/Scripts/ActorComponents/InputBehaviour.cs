using UnityEngine;

public abstract class InputBehaviour : MonoBehaviour {
    private bool _acceptInput = true;
    private bool _behaviourEnabled = true;

    public void ToggleInput() {
        _acceptInput = !_acceptInput;
    }

    public void ToggleBehaviour() {
        _behaviourEnabled = !_behaviourEnabled;
    }

    public void DisableInput() {
        _acceptInput = false;
    }

    public void EnableInput() {
        _acceptInput = true;
    }

    protected bool ShouldProcessInput() {
        return _acceptInput;
    }

    public void DisableBehaviour() {
        _behaviourEnabled = false;
    }

    public void EnableBehaviour() {
        _behaviourEnabled = true;
    }

    protected bool IsBehaviourEnabled() {
        return _behaviourEnabled;
    }
}
