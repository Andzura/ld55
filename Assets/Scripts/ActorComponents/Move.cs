using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using Utils;

[RequireComponent(typeof(Rigidbody2D))]
public class Move : InputBehaviour {
    [field: SerializeField, Range(0, 100f)]
    public float MaxSpeed { get; set; } = 5f; //9.2

    [field: SerializeField, Range(0, 100f)]
    public float GroundAcceleration { get; private set; } = 35f; //80

    [field: SerializeField, Range(0, 100f)]
    public float GroundDeceleration { get; private set; } = 80f; //76

    [field: SerializeField, Range(0, 100f)]
    public float TurnSpeed { get; private set; } = 80f; //76

    [field: SerializeField, Range(0, 1f)]
    private float _deadZone = 0f;

    public Vector2 Velocity {
        get { return _body.velocity; }
    }

    // required Component
    private Rigidbody2D _body;

    // internal State
    private Vector2 _direction;
    private Vector2 _targetVelocity;
    private float _acceleration;

    [HideInInspector]
    public float CurrentMaxSpeed;

    [HideInInspector]
    public float CurrentGroundAcceleration;

    [HideInInspector]
    public float CurrentGroundDeceleration;

    [HideInInspector]
    public float CurrentTurnSpeed;

    void Awake() {
        _body = GetComponent<Rigidbody2D>();

        //Top down shooter : No gravity
        _body.gravityScale = 0f;
        ResetValues();
    }

    public void ResetValues() {
        CurrentMaxSpeed = MaxSpeed;
        CurrentGroundAcceleration = GroundAcceleration;
        CurrentGroundDeceleration = GroundDeceleration;
        CurrentTurnSpeed = TurnSpeed;
    }

    void OnValidate() {
        ResetValues();
    }

    public void OnMovement(Vector2 direction) {
        if (!ShouldProcessInput())
            return;
        if (direction.magnitude >= 1f - _deadZone) {
            _direction = direction.normalized;
        }
        else {
            _direction = direction;
        }
        _targetVelocity = _direction * CurrentMaxSpeed;
    }

    void FixedUpdate() {
        if (!IsBehaviourEnabled()) {
            return;
        }
        Vector2 tmpVel = Velocity;
        float maxSpeedChange = 0f;
        //
        // TODO: Sprite facing
        //
        if (_direction.magnitude > _deadZone) {
            _acceleration = CurrentGroundAcceleration;
        }
        else {
            _targetVelocity = Vector2.zero;
            _acceleration = CurrentGroundDeceleration;
        }

        maxSpeedChange = _acceleration * Time.deltaTime;

        /* No equivalent for Top down movement?
        if (
            Mathf.Abs(tmpVel.x) > Mathf.Abs(_targetVelocity.x)
            && Mathf.Abs(_targetVelocity.x) > 0.01f
            && Mathf.Sign(_targetVelocity.x) == Mathf.Sign(tmpVel.x)
            && !_ground.IsOnGround()
        )
        {
            // we won't slow down the player if they go faster than our top  speed
            // *AND* they're not changing direction/stopping
            maxSpeedChange = 0f;
        }
        //*/

        tmpVel = Vector2.MoveTowards(tmpVel, _targetVelocity, maxSpeedChange);

        // FIXME
        // Remove this dirty if

        if (GetComponent<Player>() != null) {
            GetComponent<Animator>().SetFloat("Speed", tmpVel.magnitude);
            int facing = Math.Sign(tmpVel.x);
        if (facing != 0) {
          GetComponent<Animator>().SetInteger("Facing", Math.Sign(tmpVel.x));
          transform.Find("Grimoire").GetComponent<Animator>().SetInteger("Facing", Math.Sign(tmpVel.x));
        }
        else
        {
          GetComponent<Animator>().SetInteger("Facing", -1);
          transform.Find("Grimoire").GetComponent<Animator>().SetInteger("Facing", -1);

        }
      }

        _body.velocity = tmpVel;
    }
}
