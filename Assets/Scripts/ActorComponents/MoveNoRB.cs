using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utils;

public class MoveNoRB : InputBehaviour {
    [field: SerializeField, Range(0, 100f)]
    public float MaxSpeed { get; set; } = 5f; //9.2

    [field: SerializeField, Range(0, 100f)]
    public float GroundAcceleration { get; private set; } = 35f; //80

    [field: SerializeField, Range(0, 100f)]
    public float GroundDeceleration { get; private set; } = 80f; //76

    [field: SerializeField, Range(0, 100f)]
    public float TurnSpeed { get; private set; } = 80f; //76

    [field: SerializeField, Range(0, 1f)]
    private float _deadZone = 0f;

    [HideInInspector]
    public Vector2 Velocity;

    // internal State
    private Vector2 _direction;
    private Vector2 _targetVelocity;
    private float _acceleration;

    [HideInInspector]
    public float CurrentMaxSpeed;

    [HideInInspector]
    public float CurrentGroundAcceleration;

    [HideInInspector]
    public float CurrentGroundDeceleration;

    [HideInInspector]
    public float CurrentTurnSpeed;

    void Awake() {
        ResetValues();
    }

    public void ResetValues() {
        CurrentMaxSpeed = MaxSpeed;
        CurrentGroundAcceleration = GroundAcceleration;
        CurrentGroundDeceleration = GroundDeceleration;
        CurrentTurnSpeed = TurnSpeed;
    }

    void OnValidate() {
        ResetValues();
    }

    public void OnMovement(Vector2 direction) {
        if (!ShouldProcessInput())
            return;
        if (direction.magnitude >= 1f - _deadZone) {
            _direction = direction.normalized;
        }
        else {
            _direction = direction;
        }
        _targetVelocity = _direction * CurrentMaxSpeed;
    }

    void FixedUpdate() {
        if (!IsBehaviourEnabled()) {
            return;
        }
        Vector2 tmpVel = Velocity;
        float maxSpeedChange = 0f;
        if (_direction.magnitude > _deadZone) {
            _acceleration = CurrentGroundAcceleration;
        }
        else {
            _targetVelocity = Vector2.zero;
            _acceleration = CurrentGroundDeceleration;
        }

        maxSpeedChange = _acceleration * Time.deltaTime;
        tmpVel = Vector2.MoveTowards(tmpVel, _targetVelocity, maxSpeedChange);

        Velocity = tmpVel;
        transform.position += (Vector3)(Velocity * Time.fixedDeltaTime);
    }
}
