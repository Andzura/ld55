using UnityEngine;
using System.Collections;
using System;
using Utils;


public class TickManager : MonoBehaviour {
    public static TickManager Instance { get; private set; }

    public static Action OnTick {
        get { return Instance._onTick; }
        set { Instance._onTick = value; }
    }
    private Action _onTick;
    private bool _slow = false;

    [SerializeField]
    private float _tickLength = 0.25f;

    [SerializeField]
    private float _slowmoStrength = 0.15f;

    private void Awake() {
        // If there is an instance, and it's not me, delete myself.

        if (Instance != null && Instance != this) {
            Destroy(this);
        }
        else {
            Instance = this;
        }

        StartCoroutine(TickCoroutine());
    }

    private IEnumerator TickCoroutine() {
        var waitCoroutine = new WaitForSeconds(_tickLength);
        while (true) {
            _onTick?.Invoke();
            yield return waitCoroutine;
        }
    }

    public static bool SlowMo(float duration) {
        return Instance._slowMo(duration);
    }

    private bool _slowMo(float duration) {
        if (!_slow) {
            _slow = true;
            StartCoroutine(SlowMoCoroutine(duration));
            return true;
        }
        return false;
    }

    IEnumerator SlowMoCoroutine(float duration) {
        float t = 0;
        float ts;
        while (t < duration / 2) {
            ts = 1f - Ease.Elastic.Out(t / (duration / 2)) * _slowmoStrength;
            Time.timeScale = ts;
            t += Time.unscaledDeltaTime;
            yield return null;
        }
        Time.timeScale = 1f - _slowmoStrength;
        while (t > 0) {
            ts = 1f - Ease.Cubic.Out(t / (duration / 2)) * _slowmoStrength;
            Time.timeScale = ts;
            t -= Time.unscaledDeltaTime;
            yield return null;
        }
        Time.timeScale = 1f;
        _slow = false;
    }
}

