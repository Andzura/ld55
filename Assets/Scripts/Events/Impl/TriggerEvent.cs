using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Event", menuName = "Event/Trigger")]
public class TriggerEvent : GameEvent<bool> { }