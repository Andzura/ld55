using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class GameEvent<T> : ScriptableObject {
    private Action<T> _action;

    public void Raise(T value) {
        _action?.Invoke(value);
    }

    public void Add(Action<T> a) {
        _action -= a; // avoid double hook
        _action += a;
    }

    public void Remove(Action<T> a) {
        _action -= a;
    }
}

