using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class Controller : ScriptableObject {
    public float HorizontalInput {
        get { return ComputeHorizontal(); }
    }
    public float VerticalInput {
        get { return ComputeVertical(); }
    }
    public bool Jump {
        get { return ComputeJump(); }
    }
    public Vector2 Aim {
        get { return ComputeAim(); }
    }

    protected abstract float ComputeHorizontal();
    protected abstract float ComputeVertical();
    protected abstract bool ComputeJump();
    protected abstract Vector2 ComputeAim();
}
