using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;


public readonly struct InputData {
    public InputData(bool p, bool r) {
        Pressed = p;
        Released = r;
    }

    public bool Pressed { get; }
    public bool Released { get; }
}

public class ControlDispatcher : MonoBehaviour {
    [SerializeField]
    public UnityEvent<float> OnHorizontalInput;

    [SerializeField]
    public UnityEvent<float> OnVerticalInput;

    [SerializeField]
    public UnityEvent<Vector2> OnMixedInput;

    [SerializeField]
    public UnityEvent<Vector2> OnAim;

    [SerializeField]
    public UnityEvent<InputData> OnJump;

    [SerializeField]
    private Controller _controller;

    private bool _jumpState = false;

    void Update() {
        _jumpState = HandleButton(_controller.Jump, _jumpState, OnJump);

        OnHorizontalInput?.Invoke(_controller.HorizontalInput);
        OnVerticalInput?.Invoke(_controller.VerticalInput);
        OnMixedInput?.Invoke(
            new Vector2(_controller.HorizontalInput, _controller.VerticalInput)
        );
        OnAim?.Invoke(_controller.Aim);
    }

    private bool HandleButton(bool btn, bool state, UnityEvent<InputData> act) {
        if (btn && !state) {
            InputData id = new InputData(true, false);
            act?.Invoke(id);
            return true;
        }
        else if (!btn && state) {
            InputData id = new InputData(false, true);
            act?.Invoke(id);
            return false;
        }
        return state;
    }
}

