using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using System;

public abstract class ActorController : MonoBehaviour {
  [SerializeField]
  public UnityEvent<Vector2> OnMove;

  [SerializeField]
  public UnityEvent<Vector2> OnAim;
  protected Vector2 _movedir;

  void Awake() { }

  private Vector2 Direction {
      get { return _movedir; }
  }

  protected virtual void FixedUpdate() {
    SetMoveDirection();
    OnMove?.Invoke(Direction);
  }

  protected abstract void SetMoveDirection();
}

