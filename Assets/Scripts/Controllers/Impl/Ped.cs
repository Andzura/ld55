using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class Ped : ActorController {
  public float AlertDistance = 5f;
  public float DisappearDistance = 20f;
  public float SafeDistance = 10f;
  private bool _isFleeing = false;
  private float _lastAngle = -1;
  private float _cooldown = 0f;
  private float _steering = 0f;
  private bool alive = true;

  IEnumerator CheckStatusCoroutine() {
    var waitCoroutine = new WaitForSeconds(0.3f);
    while (alive) {
      float d = Vector2.Distance(Player.GetTransform().position, transform.position);
      if (d < AlertDistance) {
        _isFleeing = true;
      }
      else if (d < SafeDistance) {
        _isFleeing = false;
      }
      else if (d > DisappearDistance) {
        Destroy(gameObject);
      }
      yield return waitCoroutine;
      if (!alive) {
        break;
      }
    }
  }

  void Awake() {
    //StartCoroutine(CheckStatusCoroutine());
  }

  void OnDestroy() {
    alive = false;
  }

  //*
  protected override void FixedUpdate() {
    base.FixedUpdate();
    float d = Vector2.Distance(Player.GetTransform().position, transform.position);
    if (d < AlertDistance) {
      _isFleeing = true;
    }
    else if (d > SafeDistance) {
      _isFleeing = false;
    }

    if (d > DisappearDistance) {
      Destroy(gameObject);
    }
    //Debug.DrawCircle(transform.position, AlertDistance, 32, Color.red);
    //Debug.DrawCircle(transform.position, DisappearDistance, 32, Color.yellow);
  }

  //*/
  //

  protected override void SetMoveDirection() {
    Vector2 dir;
    if (_isFleeing) {
      dir = transform.position - Player.GetTransform().position;
      dir = dir.normalized;
      transform.localRotation = Quaternion.Euler(
        0,
        0,
        Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg
      );
    }
    else {
      float angle = _lastAngle;
      if (_lastAngle <= 0) {
        angle = Random.value * 360;
      }

      if (_cooldown > 0f) {
        _cooldown -= Time.fixedDeltaTime;
        angle += _steering;
      }
      else {
        var r = Random.value;
        if (r <= 0.5f) {
          _steering = 0f;
          _cooldown = Random.value * 0.3f;
        }
        else {
          _steering = (Random.value - 0.5f) * 20f;
          _cooldown = Random.value * 1f;
        }
      }
      _lastAngle = angle % 360;
      transform.localRotation = Quaternion.Euler(0, 0, _lastAngle);
      dir = new Vector2(
        Mathf.Cos(angle * Mathf.Deg2Rad),
        Mathf.Sin(angle * Mathf.Deg2Rad)
      );
      dir *= 0.2f;
    }
    _movedir = dir;
  }
}

