using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using Utils;

public class Shielder : ActorController {
    public float KeepDistance = 5f;
    public float DisappearDistance = 30f;

    [SerializeField]
    private GameObject _shield;

    private bool _isFleeing = false;
    private bool _alive = true;
    private Transform _playerTransform;

  public bool shouldMove = true;

    IEnumerator CheckStatusCoroutine() {
        var waitCoroutine = new WaitForSeconds(0.3f);
        while (_alive) {
            float d = Vector2.Distance(Player.GetTransform().position, transform.position);
            if (d > DisappearDistance) {
                Destroy(gameObject);
            }
            yield return waitCoroutine;
            if (!_alive) {
                break;
            }
        }
    }

    void Start() {
        _playerTransform = Player.GetTransform();
    }

    void Awake() {
        //StartCoroutine(CheckStatusCoroutine());
    }

    void OnDestroy() {
        _alive = false;
    }

    //*
    protected override void FixedUpdate() {
        if (!shouldMove)
          return;
        base.FixedUpdate();
        float d = Vector2.Distance(Player.GetTransform().position, transform.position);
        if (d > DisappearDistance) {
            Destroy(gameObject);
        }
    //Debug.DrawCircle(transform.position, AlertDistance, 32, Color.red);
    //Debug.DrawCircle(transform.position, DisappearDistance, 32, Color.yellow)
    //;

    GetComponent<Animator>().SetFloat("Speed", GetComponent<Move>().Velocity.magnitude);
    int facing = System.Math.Sign((transform.position-_playerTransform.position).x);
    if (facing != 0)
    {
      GetComponent<Animator>().SetInteger("Facing", facing);
    }
  }

    //*/
    //

    protected override void SetMoveDirection() {
        Vector2 dir;
        if (_isFleeing) {
            dir = transform.position - Player.GetTransform().position;
            dir = dir.normalized;
            transform.localRotation = Quaternion.Euler(
                0,
                0,
                Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg
            );
        }
        else {
            dir = _playerTransform.position - transform.position;

            transform.localRotation = Quaternion.Euler(
                0,
                0,
                Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg
            );
            float d = Vector2.Distance(_playerTransform.position, transform.position);
            if (Mathf.Abs(d - KeepDistance) < 0.3f) {
                dir = Vector2.zero;
            }
            else {
                float forward = d >= KeepDistance ? 1 : -1;
                dir = forward * dir;
            }
        }
        _movedir = dir;
    }
}

