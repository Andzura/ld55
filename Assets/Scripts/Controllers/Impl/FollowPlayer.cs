using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class FollowPlayer : ActorController {
    protected override void SetMoveDirection() {
        _movedir = (Player.GetTransform().position - transform.position).normalized;
    }
}

