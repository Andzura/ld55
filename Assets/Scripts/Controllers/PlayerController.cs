using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Adz
{
    [CreateAssetMenu(fileName = "PlayerController", menuName = "Controller/PlayerController")]
    public class PlayerController : Controller
    {
        protected override float ComputeHorizontal()
        {
            return Input.GetAxisRaw("Horizontal");
        }

        protected override float ComputeVertical()
        {
            return Input.GetAxisRaw("Vertical");
        }

        protected override bool ComputeJump()
        {
            return Input.GetButton("Jump");
        }

        protected override Vector2 ComputeAim()
        {
            return Camera.main.ScreenToWorldPoint(Input.mousePosition);
        }
    }
}
