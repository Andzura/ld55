using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using UnityEditor;
using UnityEngine;

public static class VectorExtension {

  public static Vector2[] ToVector2Array(this Vector3[] v3) {
    return System.Array.ConvertAll<Vector3, Vector2> (v3, GetV3fromV2);
	}

	public static Vector2 GetV3fromV2 (Vector3 v3)
	{
		return new Vector2 (v3.x, v3.y);
	}

  public static Vector2[] TranslateAll(this Vector2[] v2, Vector2 translation) {
    return System.Array.ConvertAll<Vector2, Vector2>(v2, v2 => Translate(v2, translation));
  }

  public static Vector2 Translate(Vector2 v2, Vector2 translation) {
    return new Vector2(v2.x + translation.x, v2.y + translation.y);
  }

  // public static Vector2[] ConvertAllToWorld(this Vector2[] v2) {
  //   return System.Array.ConvertAll<Vector2, Vector2>(v2, ConvertToWorld);
  // }

  // public static Vector2 ConvertToWorld(Vector2 v2) {
  //   Debug.Log("---------------");
  //   Debug.Log(v2);
  //   Vector2 converted = Camera.main.ScreenToWorldPoint(v2);
  //   Debug.Log(converted);
  //   return converted;
  // }

  public static Vector2[] Resize(this Vector2[] positions) {
    //Find "bounding boxes"
    float x1, x2, y1, y2;
    x1 = y1 = float.PositiveInfinity;
    x2 = y2 = float.NegativeInfinity;

    foreach (var position in positions) {
      if (position.x < x1) {
        x1 = position.x;
      }
      if (position.x > x2) {
        x2 = position.x;
      }

      if (position.y < y1) {
        y1 = position.y;
      }
      if (position.y > y2) {
        y2 = position.y;
      }
    }

    float sizex = Mathf.Abs(x2 - x1);
    float sizey = Mathf.Abs(y2 - y1);

    //Scriptable Objects ??
    const float TARGET_SIZEX = 10;
    const float TARGET_SIZEY = 10;

    float ratiox = TARGET_SIZEX / sizex;
    float ratioy = TARGET_SIZEY / sizey;

    for(int i = 0; i < positions.Length; i++) {
      positions[i].x = (positions[i].x - x1) * ratiox;
      positions[i].y = (positions[i].y - y1) * ratioy;
    }

    return positions;
  }

  public static NativeArray<Vector2> Resize(this NativeArray<Vector2> positions) {
    //Find "bounding boxes"
    float x1, x2, y1, y2;
    x1 = y1 = float.PositiveInfinity;
    x2 = y2 = float.NegativeInfinity;

    foreach (var position in positions) {
      if (position.x < x1) {
        x1 = position.x;
      }
      if (position.x > x2) {
        x2 = position.x;
      }

      if (position.y < y1) {
        y1 = position.y;
      }
      if (position.y > y2) {
        y2 = position.y;
      }
    }

    float sizex = Mathf.Abs(x2 - x1);
    float sizey = Mathf.Abs(y2 - y1);

    //Scriptable Objects ??
    const float TARGET_SIZEX = 10;
    const float TARGET_SIZEY = 10;

    float ratiox = TARGET_SIZEX / sizex;
    float ratioy = TARGET_SIZEY / sizey;
    Vector2 t = new Vector2(x1, y1);
    Vector2 r = new Vector2(ratiox, ratioy);
    for (int i = 0; i < positions.Length; i++) {
      positions[i] -= t;
      positions[i] *= r;
    }

    return positions;
  }



  public static Vector2 Barycentre(this Vector2[] positions) {
    float accX = 0;
    float accY = 0;

    foreach(Vector2 position in positions) {
      accX += position.x;
      accY += position.y;
    }

    return new Vector2(accX / positions.Count(), accY / positions.Count());
  }

  public static bool IsInArea(this Vector2 position, Vector2 center, float rayon) {
    float distance = position.DistanceTo(center);

    if (distance <= rayon) {
      return true;
    } else {
      return false;
    }
  }

  public static float DistanceTo(this Vector2 from, Vector2 to) {
    return (float) (Math.Sqrt(Math.Pow(from.x - to.x, 2) + Math.Pow(from.y - to.y, 2)));
  }

}