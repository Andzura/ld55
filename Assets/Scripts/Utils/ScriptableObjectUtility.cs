﻿using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
internal class ScriptableObjectUtility {
  public static void CreateLineObject(string glyphName, Vector2[] points) {
    LineObject lo = ScriptableObject.CreateInstance<LineObject>();
    lo.GlyphName = glyphName;
    lo.PointsLists = points.Resize();

    string path = "Assets/Resources/Glyphs/" + glyphName + ".asset";
    AssetDatabase.CreateAsset(lo, path);
    AssetDatabase.SaveAssets();
    AssetDatabase.Refresh();
    EditorUtility.FocusProjectWindow();
    Selection.activeObject = lo;
  }
}
