namespace Utils
{
    // largely inspired by SenLague VidTools Ease functions (https://github.com/SebLague/VidTools/blob/main/Assets/VidTools/Vis/Ease.cs)
    public static class Ease
    {
        public static class Exponential
        {
            public static float In(float t) => Expo(10 * Clamp01(t) - 10);

            public static float Out(float t) => 1 - Expo(-10 * Clamp01(t));
        }

        public static class Quartic
        {
            public static float In(float t) => Quart(Clamp01(t));

            public static float Out(float t) => 1 - Quart(1 - Clamp01(t));

            public static float InOut(float t)
            {
                if (t < 0.5f)
                {
                    return 8 * Quart(t);
                }
                else
                {
                    return 1 - Quart(-2 * t + 2) / 2;
                }
            }
        }

        public static class Cubic
        {
            public static float In(float t) => Cube(Clamp01(t));

            public static float Out(float t) => 1 - Cube(1 - Clamp01(t));

            public static float InOut(float t) => t < 0.5f ? 4 * Cube(t) : 1 - Cube(-2 * t + 2) / 2;
        }

        public static class Elastic
        {
            public static float Out(float t)
            {
                if (t >= 1)
                    return 1;
                if (t <= 0)
                    return 0;
                return Expo(-10 * t)
                        * System.MathF.Sin((t * 10 - 0.75f) * (2 * System.MathF.PI / 3))
                    + 1;
            }
        }

        public static float Linear(float t) => Clamp01(t);

        static float Clamp01(float t) => System.Math.Clamp(t, 0, 1);

        static float Square(float x) => x * x;

        static float Cube(float x) => x * x * x;

        static float Quart(float x) => x * x * x * x;

        static float Expo(float x) => System.MathF.Pow(2, x);

        static float Lerp(float a, float b, float t) => a * (1 - Clamp01(t)) + b * Clamp01(t);
    }
}
