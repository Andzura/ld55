using UnityEngine;
using System.Collections;


public class OSLog : MonoBehaviour {
    string _msg = "Nothing";
    public static OSLog Instance { get; private set; }

    private void Awake() {
        // If there is an instance, and it's not me, delete myself.

        if (Instance != null && Instance != this) {
            Destroy(this);
        }
        else {
            Instance = this;
        }
    }

    void Start() {
        Debug.Log("Started up logging.");
    }

    public static void ReplaceMessage(string msg) {
        Instance._msg = msg;
    }

    void OnGUI() {
        GUI.skin.label.fontSize = 72;
        GUILayout.BeginArea(new Rect(0, 0, 400, Screen.height));
        GUILayout.Label(_msg);
        GUILayout.EndArea();
    }
}