using System;
using System.Collections;
using UnityEngine;
using System.Linq;
using Unity.Collections;
using Unity.Jobs;
using System.Collections.Generic;

public static class LineComparator {

  public static string Process(NativeArray<Vector2> positions) {
    return FindMatching(positions);
  }
  private struct Segment {
    public Vector2 a;
    public Vector2 b;
  }

  private static string FindMatching(NativeArray<Vector2> positions) {
    var currentGlyph = SpellFinder.Instance.GetCurrentGlyph();
    LineObject[] glyphs;
    if (currentGlyph != null) {
      glyphs = currentGlyph.NextGlyphs.ToArray();
    }
    else {
      glyphs = ResourcesLoader.Instance.Glyphs;
    }

    positions.Resize();

    float minDiff = -1;
    LineObject minGlyph = null;
    foreach (LineObject glyph in glyphs) {
      glyph.PointsLists = glyph.PointsLists.Resize();
      NativeArray<Vector2> cpyGlyph = new NativeArray<Vector2>(glyph.PointsLists, Allocator.Temp);
      for (int i = 0; i < glyph.PointsLists.Count() - 1; i++) {
        Debug.DrawLine(glyph.PointsLists[i], glyph.PointsLists[i + 1], Color.red);
      }
      for (int i = 0; i < positions.Count() - 1; i++) {
        Debug.DrawLine(positions[i], positions[i + 1], Color.green);
      }


      float diffValue = Compare(cpyGlyph, positions);

      if (diffValue < minDiff || minDiff == -1) {
        minDiff = diffValue;
        minGlyph = glyph;
      }
      cpyGlyph.Dispose();
    }

    if (minDiff < 20 && minDiff != -1) {
      return minGlyph.GlyphName;
    }
    else {
      return "";
    }
  }

  private static float Compare(NativeArray<Vector2> reference, NativeArray<Vector2> drawing) {
    Vector2 firstReferencePoint = reference[0];
    Vector2 firstDrawingPoint = drawing[0];



    float diff = 0;
    float reverseDiff = 0;

    diff = CompareOneFormToRef(reference, drawing);
    reverseDiff = CompareOneFormToRef(drawing, reference);

    float result = (diff > reverseDiff) ? diff : reverseDiff;

    return result;
  }

  private static float CompareOneFormToRef(NativeArray<Vector2> reference, NativeArray<Vector2> drawing) {
    float sqrDistAcc = 0f;
    float length = 0f;

    Vector2 prevPoint = drawing[0];

    foreach (Vector2 drawingPoint in WalkAlong(drawing)) {
      sqrDistAcc += SqrDistanceToLine(reference, drawingPoint);
      length += Vector2.Distance(drawingPoint, prevPoint);

      prevPoint = drawingPoint;
    }

    return sqrDistAcc / length;
  }

  private static IEnumerable<Vector2> WalkAlong(IEnumerable<Vector2> line, float maxStep = .1f) {
    using (var lineEnum = line.GetEnumerator()) {
      if (!lineEnum.MoveNext()) {
        yield break;
      }

      var position = lineEnum.Current;

      while (lineEnum.MoveNext()) {
        var target = lineEnum.Current;
        while (position != target) {
          yield return position = Vector2.MoveTowards(position, target, maxStep);
        }
      }
    }
  }

  private static float SqrDistanceToLine(NativeArray<Vector2> line, Vector2 point) {
    return ListSegments(line).Select(seg => SqrDistanceToSegment(seg.a, seg.b, point)).Min();
  }

  private static float SqrDistanceToSegment(Vector2 segmentStart, Vector2 segmentEnd, Vector2 point) {
    var projected = ProjectPointOnLineSegment(segmentStart, segmentEnd, point);
    return (projected - point).sqrMagnitude;
  }

  private static Vector2 ProjectPointOnLineSegment(Vector2 segmentStart, Vector2 segmentEnd, Vector2 point) {
    Vector2 segmentVector = segmentStart - segmentEnd;
    Vector2 projectedPoint = ProjectPointOnLine(segmentStart, segmentVector.normalized, point);

    switch (PointOnWhichSideOfLineSegment(segmentStart, segmentEnd, projectedPoint)) {
      case 0:
        return projectedPoint;
      case 1:
        return segmentStart;
      case 2:
        return segmentEnd;
      default:
        //output is invalid
        return Vector2.zero;
    }

  }

  private static int PointOnWhichSideOfLineSegment(Vector2 segmentStart, Vector2 segmentEnd, Vector2 projectedPoint) {
    Vector2 lineVec = segmentEnd - segmentStart;
    Vector2 pointVec = projectedPoint - segmentStart;

    if (Vector2.Dot(pointVec, lineVec) > 0) {
      return pointVec.magnitude <= lineVec.magnitude ? 0 : 2;
    }
    else {
      return 1;
    }
  }

  private static Vector2 ProjectPointOnLine(Vector2 segmentStart, Vector2 segmentVector, Vector2 point) {
    //get vector from point on line to point in space
    Vector2 linePointToPoint = point - segmentStart;
    float t = Vector2.Dot(linePointToPoint, segmentVector);
    return segmentStart + segmentVector * t;
  }


  private static IEnumerable<Segment> ListSegments(IEnumerable<Vector2> line) {
    using (var segmentStart = line.GetEnumerator())
    using (var segmentEnd = line.GetEnumerator()) {
      segmentEnd.MoveNext();

      while (segmentEnd.MoveNext()) {
        segmentStart.MoveNext();

        yield return new Segment { a = segmentStart.Current, b = segmentEnd.Current };
      }
    }
  }
  public static CompareJob CreateCompareJob(Vector2[] lastPositions) {
    NativeArray<Vector2> naPositions = new NativeArray<Vector2>(lastPositions, Allocator.Persistent);
    NativeText output = new NativeText(Allocator.Persistent);
    CompareJob cj = new CompareJob() {
      Positions = naPositions,
      output = output
    };
    return cj;
  }

}

public struct CompareJob : IJob {
  public NativeArray<Vector2> Positions;
  public NativeText output;
  public void Execute() {
    output.Append(LineComparator.Process(Positions));
  }
}