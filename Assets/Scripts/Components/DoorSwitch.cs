using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorSwitch : MonoBehaviour {

  [SerializeField]
  private SpriteRenderer _graphics;

  [SerializeField]
  private Collider2D _collider;


  public void KeyOpen() {
    _graphics.enabled = false;
    _collider.enabled = false; 
  }

  public void KeyClose() {
    _graphics.enabled = true;
    _collider.enabled = true;
  }
}
