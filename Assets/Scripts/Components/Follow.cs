using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class Follow : SingletonBehaviour {
  [SerializeField]
  public List<Transform> ObjectToFit = new List<Transform>();

  public bool InstantFollow = false;

  [field: SerializeField, Range(0, 10f)]
  private float _extrapolate = 0f;

  [field: SerializeField, Range(0.1f, 1f)]
  private float _jumpspeed = 0.2f;

  [field: SerializeField, Range(1f, 10f)]
  private float _minCameraSize = 8f;

  [field: SerializeField]
  private float minY = -1f;
  [field: SerializeField]
  private float maxY = 143f;
  private Camera _camera;
  private Vector3 _previousTargetPosition;
  private float _currentExtrapolate = 0f;
  private Transform _player;

  // Start is called before the first frame update
  void Awake() {
    if (Instance != null) {
      Destroy(gameObject);
    }
    else {
      Instance = this;
      DontDestroyOnLoad(gameObject);
    }
    _camera = GetComponent<Camera>();
    _previousTargetPosition = (ObjectToFit.Count > 0 ? ObjectToFit[0].position : Vector2.zero);
    _currentExtrapolate = _extrapolate;
  }

  // Update is called once per frame
  void FixedUpdate() {
    if (ObjectToFit.Count > 0) {


      //Zoom level
      float nextCameraSize = ComputeDistanceBetweenTargets();
      if (Math.Abs(_camera.orthographicSize - nextCameraSize) > _jumpspeed)
        _camera.orthographicSize += Math.Sign(nextCameraSize - _camera.orthographicSize) * _jumpspeed * 0.3f;

      Vector3 objCenter = Vector3.zero;
      foreach (Transform t in ObjectToFit) {
        objCenter += t.position;
      }

      objCenter /= ObjectToFit.Count;
      Vector2 objVelocity = (objCenter - _previousTargetPosition);
      Vector2 nextPos = (Vector2)(objCenter) + objVelocity * _extrapolate;


      nextPos.y = Math.Min(maxY - _camera.orthographicSize, Math.Max(minY + _camera.orthographicSize, nextPos.y));
      if (Vector3.Distance(transform.position, nextPos) > 10.001f) {
        if (!InstantFollow) {
          transform.position = Vector3.MoveTowards(transform.position, new Vector3(nextPos.x, nextPos.y, -10), _jumpspeed * 2f);
        }
        else {
          Debug.Log("MDRR");
          transform.position = new Vector3(nextPos.x,  nextPos.y, -10);
        }
      }
      else {
        transform.position = new Vector3(nextPos.x, nextPos.y, -10);
      }

      _previousTargetPosition = objCenter;
    }
  }

  internal void ClearObjects() {
    ObjectToFit.Clear();
  }

  internal void AddObjectToFollow(Transform transform) {
    ObjectToFit.Add(transform);
  }

  public void SnapFollow() {
    InstantFollow = true;
    _extrapolate = 0;
  }

  public void StopSnapFollow() {
    InstantFollow = false;
    _extrapolate = _currentExtrapolate;
  }
  private float ComputeDistanceBetweenTargets() {
    if (ObjectToFit.Count < 2)
      return _minCameraSize;
    float distance = _minCameraSize;
    foreach (var obj1 in ObjectToFit) {
      foreach (var obj2 in ObjectToFit) {
        float new_distance = Vector3.Distance(obj1.position, obj2.position);
        distance = Math.Max(distance, new_distance * 0.5f);
      }
    }
    return distance;
  }

}