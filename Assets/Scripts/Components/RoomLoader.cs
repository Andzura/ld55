using System;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class RoomLoader : MonoBehaviour {
  [Serializable]
  public struct NamedRoom {
    public string name;
    public GameObject room;
  }

  [SerializeField]
  private string _startRoomName;
  [SerializeField]
  private GameObject _startRoomObject;

  [Header("Setup current & next room at start up")]
  public GameObject CurrentRoom;
  [SerializeField]
  private string _currentRoomName;
  public GameObject PreviousRoom;
  [SerializeField]
  private string _previousRoomName;
  public GameObject NextRoom;
  [SerializeField]
  private string _nextRoomName;


  public static RoomLoader Instance { get; private set; }

  [SerializeField]
  public List<NamedRoom> RoomList = new();

  private Dictionary<string, GameObject> _roomDictionary = new Dictionary<string, GameObject>();

  private void Awake() {
    if(Instance == null) {
      Instance = this;
      CurrentRoom = _startRoomObject;
      _currentRoomName = _startRoomName;
      _previousRoomName = _startRoomName;
    }
    else {
      Destroy(this);
    }
  }

  private void OnValidate() {
    _roomDictionary = RoomList.ToDictionary(x => x.name, x => x.room);
  }

  public bool LoadRoom(string roomName, Vector3 position) {
    if (roomName == _currentRoomName || roomName == _previousRoomName || roomName == _nextRoomName) {
      Debug.Log(roomName + " already loaded");
      return false;
    }

    var prefabRoom = _roomDictionary.GetValueOrDefault(roomName, null);
    if (prefabRoom == null) {
      Debug.Log(roomName + " not found...");
      return false;
    }

    GameObject room = Instantiate(prefabRoom, position, Quaternion.identity);
    NextRoom = room;
    _nextRoomName = roomName;
    return true;
  }

  public void SwitchRoom(string roomName) {
    if (_currentRoomName == roomName)
    {
      Debug.Log(roomName + " - already in this room");
      return;
    }
    if (_previousRoomName == roomName)
    {
      Destroy(NextRoom);
      _nextRoomName = "";
      _previousRoomName = _currentRoomName;
      _currentRoomName = roomName;
      var tmp = PreviousRoom;
      PreviousRoom = CurrentRoom;
      CurrentRoom = tmp;
      return;
    }
    if (_nextRoomName == roomName)
    {
      Destroy(PreviousRoom);
      _nextRoomName = "";
      _previousRoomName = _currentRoomName;
      PreviousRoom = CurrentRoom;
      _currentRoomName = roomName;
      CurrentRoom = NextRoom;
      return;
    }
    //We should not reach this far into code
    Debug.Log("Error in RoomLoader.SwitchRoom : roomName doesn't correspond to any loaded room...");
  }

  internal void ReSetup(string roomName, Vector3 roomPosition) {
    _currentRoomName = roomName;
    _previousRoomName = roomName;
    var prefabRoom = _roomDictionary.GetValueOrDefault(roomName, null);
    GameObject room = Instantiate(prefabRoom, roomPosition, Quaternion.identity);
  }

  internal void UnloadAll() {
    Destroy(PreviousRoom);
    Destroy(CurrentRoom);
    Destroy(NextRoom);
    _currentRoomName = "";
    _previousRoomName = "";
    _nextRoomName = "";
    PreviousRoom = null;
    CurrentRoom = null;
    NextRoom = null;
  }
}
