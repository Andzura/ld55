using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(Collider2D))]
public class InteractDoor : MonoBehaviour, IInteractable {
  public Sprite OpenedSprite;
  public bool fireInteractable = false;
  public bool iceInteractable = false;

  private bool _opened = false;

  public void Freeze(float duration) {
    if (!_opened && iceInteractable) {
      GetComponent<Collider2D>().enabled = false;
      GetComponent<SpriteRenderer>().sprite = OpenedSprite;
    }
  }

  public void Fire(float duration) {
    if (!_opened && fireInteractable) {
      GetComponent<Collider2D>().enabled = false;
      GetComponent<SpriteRenderer>().sprite = OpenedSprite;
    }
  }
}
