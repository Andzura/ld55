using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;

public class SceneTrigger : Trigger
{
  public SceneAsset scene;
  protected override void OnTrigger(GameObject go)
  {
    MusicManager.Instance.StopTrack();
    SceneManager.LoadScene(scene.name);
  }
}
