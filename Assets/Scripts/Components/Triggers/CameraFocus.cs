using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFocus : Trigger {
  public List<Transform> targets;

  protected override void OnTrigger(GameObject go) {
    if(Triggered) return;

    Camera.main.GetComponent<Follow>().ObjectToFit.AddRange(targets);
    base.OnTrigger(go);
  }
}

