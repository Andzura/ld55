using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnterRoomTrigger : Trigger {

  public string RoomName;

  public List<Transform> PointsToFollow;
  public List<Enemy> EnemiesToActivate;

  private int _followNumber;
  protected override void OnTrigger(GameObject go) {
    RoomLoader.Instance.SwitchRoom(RoomName);
    foreach(var enemy in EnemiesToActivate)
    {
      if (enemy is Rock && enemy != null)
        enemy.enabled = true;
      else if (enemy is Archer && enemy != null)
        enemy.enabled = true;
    }
    foreach(var camera_pt in PointsToFollow)
    {
      FindFirstObjectByType<Follow>().ObjectToFit.Add(camera_pt);
    }
  }

  protected override void LeaveTrigger(GameObject go)
  {
    LeaveRoom();
  }

  public void LeaveRoom()
  {
    foreach (var camera_pt in PointsToFollow)
    {
      FindFirstObjectByType<Follow>().ObjectToFit.Remove(camera_pt);
    }
  }

}
