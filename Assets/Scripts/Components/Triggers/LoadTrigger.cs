using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadTrigger : Trigger {

  public string ZoneName = "";

  public Vector3 Position;

  protected override void OnTrigger(GameObject go) {
      if(!RoomLoader.Instance.LoadRoom(ZoneName, Position))
        return;
  
    base.OnTrigger(go);
  }
}
