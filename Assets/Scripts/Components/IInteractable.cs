using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{

  abstract void Fire(float duration);
  abstract void Freeze(float duration);
}
