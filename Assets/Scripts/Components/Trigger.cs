using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Trigger : MonoBehaviour {

  public bool Triggered = false;

  protected virtual void OnTrigger(GameObject go) {
    Triggered = true;
  }
  protected virtual void LeaveTrigger(GameObject go)
  {
    Triggered = false;
  }

  private void OnTriggerEnter2D(Collider2D other) {
    OnTrigger(other.gameObject);
  }

  private void OnTriggerExit2D(Collider2D other)
  {
    LeaveTrigger(other.gameObject);
  }

}
