using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointObject : MonoBehaviour {
  public Vector3 RespawnPoint;
  public string SceneName;
  public Animator Anim;
  public Sprite ActivatedSprite;

  public void Save() {
    Player.Instance.SetCheckpoint(new Player.Checkpoint() {
      position = RespawnPoint,
      sceneName = SceneName
    });
    Anim.SetTrigger("activated");
  }
}
