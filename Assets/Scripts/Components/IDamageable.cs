using Unity.Collections;
using System.Collections;
using UnityEngine;
using Unity.Jobs;
using System.Collections.Generic;
using System.Numerics;

interface IDamageable {

  public float Health {
    get;
    set;
  }

  public float MaxHealth {
    get;
    set;
  }

  void TakeDamage(float damage) {
    Health -= damage;

    if (Health <= 0) {
      Death();
    }

    StartFlashingCoroutine();
  }

  public void Death();

  public IEnumerator Flash(float time, float intervalTime) {
    Color[] colors = {Color.white, Color.red};
    float elapsedTime = 0f;
    int index = 0;

    while(elapsedTime < time )
    {
      Renderer().material.color = colors[index % 2];

      elapsedTime += Time.deltaTime;
      index++;
      yield return new WaitForSeconds(intervalTime);
    }
    Renderer().material.color = Color.white;
    SpecialActionAfterDamage();
  }

  public SpriteRenderer Renderer();

  public abstract void StartFlashingCoroutine();

  public abstract void SpecialActionAfterDamage();
}