using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Collections;
using Unity.Jobs;
using Unity.VisualScripting;
using UnityEngine;

public class Tracer : MonoBehaviour {
  public static Tracer Instance { get; private set; }
  public bool save = false;
  public string assetName = "Glyph";

  private GameObject _currentLine;
  private GameObject _dummyLine;
  private List<GameObject> _lines = new List<GameObject>();
  private Coroutine _tracing;
  private Vector2[] lastPositions;

  private bool _buttonState = false;

  private void Awake() {
    if (Instance != null) {
      Destroy(gameObject);
    }
    else {
      Instance = this;
      DontDestroyOnLoad(gameObject);
    }

  }
  private void Update() {

    if (!_buttonState && Input.GetMouseButtonDown(0)) {
      _buttonState = true;
    }
    else if (_buttonState && Input.GetMouseButtonUp(0)) {
      _buttonState = false;
    }
  }

  private void FixedUpdate() {
    if (save) {
      if (lastPositions != null) {
        ScriptableObjectUtility.CreateLineObject(assetName, lastPositions.Resize());
        save = false;
      }
    }

    if (_buttonState && _currentLine == null) {
      StartLine();
    }

    if (!_buttonState && _currentLine != null) {
      FinishLine();
    }
  }

  private void StartLine() {
    if (_tracing != null) {
      StopCoroutine(_tracing);
    }
    _tracing = StartCoroutine(DrawLine());
  }

  private void FinishLine() {
    StopCoroutine(_tracing);
    if (_dummyLine == null) {
      Destroy(_currentLine.gameObject);
      _currentLine = null;
    }
    LineRenderer ln = _dummyLine.GetComponent<LineRenderer>();
    ln.Simplify(0.1f);
    Vector3[] positions = new Vector3[ln.positionCount];
    ln.GetPositions(positions);
    if (ln.positionCount <= 2) {
      Debug.Log("juste un click, On tape");
      Game.Instance.CastSpell("tatane", Camera.main.ScreenToWorldPoint(Input.mousePosition));
      _lines.Clear();
      Destroy(ln.gameObject);
      Destroy(_currentLine.gameObject);
      _currentLine = null;
      _dummyLine = null;
      return;
    }

    lastPositions = positions.ToVector2Array();
    _lines.Add(_currentLine);
    SpellFinder.Instance.EnqueueJob(lastPositions);
    Destroy(_dummyLine);
    _dummyLine = null;
    _currentLine = null;
    StartCoroutine("CooldownDraw");
  }

  private IEnumerator DrawLine() {
    _currentLine = Instantiate(Resources.Load("Prefabs/Line"), Vector3.zero, Quaternion.identity, GameObject.FindGameObjectWithTag("TraceCanva").transform) as GameObject;
    _dummyLine = Instantiate(Resources.Load("Prefabs/DummyLine")) as GameObject;

    _currentLine.transform.localPosition = new Vector2(0, 0);

    LineRenderer lineRenderer = _currentLine.GetComponent<LineRenderer>();
    LineRenderer dummyRenderer = _dummyLine.GetComponent<LineRenderer>();

    lineRenderer.positionCount = 0;
    dummyRenderer.positionCount = 0;

    while (true) {
      if (_dummyLine == null) {
        Destroy(_currentLine.gameObject);
        _currentLine = null;
        break;
      }
      Vector3 mousePosition = Input.mousePosition;
      Vector3 position = mousePosition;
      position.x -= Screen.width / 2;
      position.y -= Screen.height / 2;
      position.z = 0;

      lineRenderer.positionCount++;
      dummyRenderer.positionCount++;

      lineRenderer.SetPosition(lineRenderer.positionCount - 1, position);
      dummyRenderer.SetPosition(dummyRenderer.positionCount - 1, Camera.main.ScreenToWorldPoint(mousePosition));

      yield return new WaitForFixedUpdate();
    }
  }

  private IEnumerator CooldownDraw() {
    yield return new WaitForSeconds(.5f);
    foreach (GameObject go in _lines) {
      go.GetComponent<LineVisual>().StartFadeOut();
      SpellFinder.Instance.SendSpell();
    }
    _lines.Clear();
  }
}
