using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class ResourcesLoader : MonoBehaviour {
  public static ResourcesLoader Instance { get; private set; }
  public LineObject[] Glyphs { get { return _glyphs.Values.Where(g => !g.IsChild).ToArray(); } }

  private Dictionary<string, LineObject>  _glyphs;
  // Start is called before the first frame update
  void Awake() {
    if (Instance == null) {
      Instance = this;
      LoadResources();
    }
    else {
      Destroy(this);
    }
  }

  // Update is called once per frame
  void Update() {

  }

  private void LoadResources() {
    _glyphs = Resources.LoadAll<LineObject>("Glyphs").ToDictionary(lo => lo.GlyphName, lo => lo);
  }

  public LineObject GetGlyph(string glyphName) {
    return _glyphs.GetValueOrDefault(glyphName, null);
  }
}
